package com.iarv.qar;

import com.iarv.qar.dao.Department;
import com.iarv.qar.dao.Employee;
import com.iarv.qar.dao.Location;
import com.iarv.qar.repository.DepartmentRepository;
import com.iarv.qar.repository.EmployeeRepository;
import com.iarv.qar.repository.LocationRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * In this class we initialize the Employee table. We save the default values
 * on the mysql DB qar and table employee
 */
@Profile("!test")
@Configuration
public class EmployeeConfig {
    @Bean
    CommandLineRunner commandLineRunner(EmployeeRepository employeeRepository,
                                        DepartmentRepository departmentRepository,
                                        LocationRepository locationRepository) {
        return args -> {
            Location location1 =new Location("South");

            Department department11=new Department(
                    "Software",
                    location1
            );

            Employee employee111 = new Employee(
                    "Arvanitis",
                    "John",
                    "Developer",
                    12,
                    LocalDate.of(2000, 11, 21),
                    2000.1234,
                    1000,
                    department11
                    );

            Employee employee112 = new Employee(
                    "Katsoy",
                    "Peter",
                    "Developer",
                    12,
                    LocalDate.of(2023, 5, 3),
                    1340.478,
                    1300,
                    department11
            );

            //////////////////////////////////////
            Department department12=new Department(
                    "Hardware",
                    location1
            );

            Employee employee121 = new Employee(
                    "Zouvelekis",
                    "Kostas",
                    "Developer",
                    17,
                    LocalDate.of(2021, 11, 21),
                    2040.1234,
                    3000,
                    department12
            );

            Employee employee122 = new Employee(
                    "Koykos",
                    "Andreas",
                    "Developer",
                    6,
                    LocalDate.of(2020, 5, 3),
                    3000,
                    1234,
                    department12
            );

            Employee employee123 = new Employee(
                    "Kokos",
                    "Nikos",
                    "Developer",
                    17,
                    LocalDate.of(2020, 5, 3),
                    1040.478,
                    100,
                    department12
            );

            /////////////////////////////////////////////
            Department department13=new Department(
                    "HR",
                    location1
            );

            Employee employee131 = new Employee(
                    "Manetas",
                    "Xristos",
                    "Social Relations",
                    4,
                    LocalDate.of(1998, 11, 21),
                    5000.1234,
                    1034,
                    department13
            );
            ////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////
            Location location2 =new Location("North");

            Department department21=new Department(
                    "Software",
                    location2
            );

            Employee employee211 = new Employee(
                    "Mpakothanashs",
                    "Kyriakos",
                    "Developer",
                    78,
                    LocalDate.of(1999, 11, 21),
                    2145.1234,
                    1053,
                    department21
            );

            //Save to repo
            locationRepository.saveAll(List.of(location1,location2));
            departmentRepository.saveAll(List.of(department11,department12,department13,department21));
            employeeRepository.saveAll(List.of(employee111,employee112,employee121,employee122,employee123,employee131,employee211));

        };
    }
}
