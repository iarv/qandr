package com.iarv.qar.controller;

import com.iarv.qar.dao.Employee;
import com.iarv.qar.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(produces = "application/xml")
public class EmployeeController {
    private EmployeeService service;

    @Autowired
    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    /**
     * Get all employees (for all depts and locations)
     * @return JSON list of all employees
     */
    @GetMapping("/getAllEmployees")
    public List<Employee> getAllEmployees() {
        return service.getAllEmployees();
    }

    /**
     * Get all employees (for a specific department)
     * @return JSON list of all employees of department
     */
    @GetMapping("/getAllEmployeesByDepartmentName/{departmentName}")
    public List<Employee> getAllEmployeesByDepartmentName(@PathVariable  String departmentName) {
        return service.getAllEmployeesByDepartmentName(departmentName);
    }

    /**
     * Get the employee for a specific Id (table primary key)
     * @param id The requested id
     * @return the employee of the specific row
     */
    @GetMapping("/getEmployeeById/{id}")
    public Employee getEmployeeById(@PathVariable Long id) {
        return service.getEmployeeById(id);
    }

    /**
     * Get all employees with the specific lastname
     * @return JSON list of all employees
     */
    @GetMapping("/getAllEmployeesByLastName/{lastName}")
    public List<Employee> getAllEmployeesByLastName(@PathVariable  String lastName) {
        return service.getAllEmployeesByLastName(lastName);
    }

    /**
     * Get all employees (for a specific job)
     * @return JSON list of all employees
     */
    @GetMapping("/getAllEmployeesByJob/{job}")
    public List<Employee> getAllEmployeesByJob(@PathVariable  String job) {
        return service.getAllEmployeesByJob(job);
    }

    /**
     * add new employee
     * @param employee the added employee as a Post body e.g.
     *    {
     *      "lastname": "Arva",
     *      "firstname": "John",
     *      "job": "Developer",
     *      "mngid": 12,
     *      "hiredate": "3900-12-20T22:00:00.000+00:00",
     *      "salary": 2000.1234,
     *      "comm": 1000.0
     *     }
     * @return Response the same info via HTTTP
     */
    @PostMapping("/addEmployee")
    public Employee addEmployee(@RequestBody Employee employee) {
        return service.addEmployee(employee);
    }

    /**
     * Delete HTTP request of an employee from DB
     * @param id the primary key
     */
    @DeleteMapping("/deleteEmployee/{id}")
    public Employee deleteEmployee(@PathVariable Long id) {
        return service.deleteEmployee(id);
    }

    /**
     * Update an employee raw
     * @param employee the already existed iten in HTTP body
     *    {
     *      "lastname": "Arva",
     *      "firstname": "Johnny",
     *      "job": "Developer",
     *      "mngid": 12,
     *      "hiredate": "3900-12-20T22:00:00.000+00:00",
     *      "salary": 2000.1234,
     *      "comm": 1000.0
     *     }
     * @param id the primary key
     */
    @PutMapping("/updateEmployee/{id}")
    public Employee updateEmployee(@RequestBody Employee employee, @PathVariable Long id) {
        return service.updateEmployee(id, employee);
    }
}