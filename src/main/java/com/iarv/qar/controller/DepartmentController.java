package com.iarv.qar.controller;

import com.iarv.qar.service.DepartmentService;
import com.iarv.qar.dao.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(produces = "application/xml")
public class DepartmentController {
    private DepartmentService service;

    @Autowired
    public DepartmentController(DepartmentService service) {
        this.service = service;
    }

    /**
     * Get all departments (for all locations)
     * @return JSON list of all departments
     */
    @GetMapping("/getAllDepartments")
    public List<Department> getAllDepartments() {
        return service.getAllDepartments();

    }

    /**
     * Get all departments (for a specific location)
     * @return JSON list of all departments of a location
     */
    @GetMapping("/getAllDepartmentsByLocationName/{locationName}")
    public List<Department> getAllDepartmentsByLocationName(@PathVariable  String locationName) {
        return service.getAllDepartmentsByLocationName(locationName);

    }

    /**
     * Get the department for a specific Id (table primary key)
     * @param id The requested id
     * @return the department of the specific row
     */
    @GetMapping("/getDepartmentById/{id}")
    public Department getDepartmentById(@PathVariable Long id) {
        return service.getDepartmentById(id);
    }

    /**
     * add new department
     * @param department the added department as a Post body e.g.
     *    {
     *      "dname": "dname15"
     *     }
     * @return Response the same info via HTTTP
     */
    @PostMapping("/addDepartment")
    public Department addDepartment(@RequestBody Department department) {
        return service.addDepartment(department);
    }

    /**
     * Delete HTTP request of an department from DB
     * @param id the primary key
     */
    @DeleteMapping("/deleteDepartment/{id}")
    public Department deleteDepartment(@PathVariable Long id) {
        return service.deleteDepartment(id);
    }

    /**
     * Update an department raw
     * @param department the already existed iten in HTTP body
     *    {
     *      "dname": "dname15"
     *     }
     * @param id the primary key
     */
    @PutMapping("/updateDepartment/{id}")
    public Department updateDepartment(@RequestBody Department department, @PathVariable Long id) {
        return service.updateDepartment(id, department);
    }
}