package com.iarv.qar.controller;

import com.iarv.qar.dao.Location;
import com.iarv.qar.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(produces = "application/xml")
public class LocationController {
    private LocationService service;

    @Autowired
    public LocationController(LocationService service) {
        this.service = service;
    }

    /**
     * Get all locations (for all depts and employees)
     * @return JSON list of all locations
     */
    @GetMapping("/getAllLocations")
    public List<Location> getAllLocations() {
        return service.getAllLocations();

    }

    /**
     * Get the location for a specific Id (table primary key)
     * @param id The requested id
     * @return the location of the specific row
     */
    @GetMapping("/getLocationById/{id}")
    public Location getLocationById(@PathVariable Long id) {
        return service.getLocationById(id);
    }

    /**
     * add new location
     * @param location the added location as a Post body e.g.
     *    {
     *         "locname" : "loc5"
     *     }
     * @return Response the same info via HTTTP
     */
    @PostMapping("/addLocation")
    public Location addEmployee(@RequestBody Location location) {
        return service.addLocation(location);
    }

    /**
     * Delete HTTP request of a location from DB
     * @param id the primary key
     */
    @DeleteMapping("/deleteLocation/{id}")
    public Location deleteLocation(@PathVariable Long id) {
        return service.deleteLocation(id);
    }

    /**
     * Update e location raw
     * @param location the new clearing cost in HTTP body
     *    {
     *          "locname" : "loc6"
     *    }
     *
     * @param id the primary key
     */
    @PutMapping("/updateLocation/{id}")
    public Location updateLocation(@RequestBody Location location, @PathVariable Long id) {
        return service.updateLocation(id, location);
    }
}