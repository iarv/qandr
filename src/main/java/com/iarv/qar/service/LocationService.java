package com.iarv.qar.service;

import com.iarv.qar.dao.Location;
import com.iarv.qar.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LocationService {
    private LocationRepository locationRepo;

    @Autowired
    public LocationService(LocationRepository productRepository){
        locationRepo =productRepository;
    }

    /**
     * Delete action on DM for a location
     * @param id the primary key
     * @return the deleted location
     */
    public Location getLocationById(Long id) {
        return locationRepo.findById(id).orElse(null);
    }

    /**
     * Upfdate the location on DB
     * @param id the table primary key
     * @param location the new location
     * @return the updated clearing cost on DB
     */
    public Location updateLocation(Long id, Location location) {
        Location existingLocation = locationRepo.findById(id).orElse(null);
        if (existingLocation != null) {
            existingLocation.setLocname(location.getLocname());
            //existingLocation.setFirstname(employee.getFirstname());
            //existingEmployee.setCost(employee.getCost());
            return locationRepo.save(existingLocation);
        }
        else{
            throw new IllegalStateException("Not found Location with id:"+id);
        }
    }

    /**
     * Delete a location from DB
     * @param id the table primary key
     */
    public Location deleteLocation(Long id) {
        boolean exists = locationRepo.existsById(id);
        if (!exists){
            throw new IllegalStateException("Not found Location with id:"+id);
        }
        else{
            Optional<Location> location = locationRepo.findById(id);
            locationRepo.deleteById(id);
            return location.get();
        }

    }

    /**
     * Get all locations form DB
     * @return all the locations as a JSON list
     */
    public List<Location> getAllLocations(){
        return locationRepo.findAll();
    }

    /**
     * Add new location
     * @param location
     * @return the new clearing cost
     */
    public Location addLocation(Location location) {
        return locationRepo.save(location);
    }

}
