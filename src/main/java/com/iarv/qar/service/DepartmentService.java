package com.iarv.qar.service;

import com.iarv.qar.dao.Department;
import com.iarv.qar.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {
    private DepartmentRepository departmentRepo;

    @Autowired
    public DepartmentService(DepartmentRepository departmentRepository){
        departmentRepo =departmentRepository;
    }

    /**
     * Delete action on DM for a department
     * @param id the primary key
     * @return the deleted department
     */
    public Department getDepartmentById(Long id) {
        return departmentRepo.findById(id).orElse(null);
    }

    /**
     * Update the department on DB
     * @param id the table primary key
     * @param department the new department
     * @return the updated department on DB
     */
    public Department updateDepartment(Long id, Department department) {
        Department existingDepartment = departmentRepo.findById(id).orElse(null);
        if (existingDepartment != null) {
            existingDepartment.setDname(department.getDname());
            //existingDepartment.setFirstname(employee.getFirstname());
            //existingEmployee.setCost(employee.getCost());
            return departmentRepo.save(existingDepartment);
        }
        else{
            throw new IllegalStateException("Not found Department with id:"+id);
        }
    }

    /**
     * Delete a department from DB
     * @param id the table primary key
     */
    public Department deleteDepartment(Long id) {
        boolean exists = departmentRepo.existsById(id);
        if (!exists){
            throw new IllegalStateException("Not found Department with id:"+id);
        }
        else{
            Optional<Department> department = departmentRepo.findById(id);
            departmentRepo.deleteById(id);
            return department.get();
        }

    }

    /**
     * Get all departments from DB
     * @return all the departments as a JSON list
     */
    public List<Department> getAllDepartments(){
        return departmentRepo.findAll();
    }

    /**
     * Get all departments from DB
     * @return all the departments as a JSON list
     */
    public List<Department> getAllDepartmentsByLocationName(String locationName){
        return departmentRepo.getAllDepartmentsByLocationName(locationName);
    }

    /**
     * Add new department
     * @param department
     * @return the new department
     */
    public Department addDepartment(Department department) {
        return departmentRepo.save(department);
    }

}
