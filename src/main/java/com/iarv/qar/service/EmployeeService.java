package com.iarv.qar.service;

import com.iarv.qar.dao.Employee;
import com.iarv.qar.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    private EmployeeRepository employeeRepo;

    @Autowired
    public EmployeeService(EmployeeRepository productRepository){
        employeeRepo =productRepository;
    }

    /**
     * Delete action on DM for a employee
     * @param id the primary key
     * @return the deleted employee
     */
    public Employee getEmployeeById(Long id) {
        return employeeRepo.findById(id).orElse(null);
    }

    /**
     * Get all departments from DB
     * @return all the departments as a JSON list
     */
    public List<Employee> getAllEmployeesByDepartmentName(String departmentName){
        return employeeRepo.getAllEmployeesByDepartmentName(departmentName);
    }

    /**
     * Get all departments from DB
     * @return all the departments as a JSON list
     */
    public List<Employee> getAllEmployeesByLastName(String lastName){
        return employeeRepo.findAllByLastname(lastName);
    }

    /**
     * Get all departments from DB
     * @return all the departments as a JSON list
     */
    public List<Employee> getAllEmployeesByJob(String job){
        return employeeRepo.findAllByJob(job);
    }

    /**
     * Upfdate the employee on DB
     * @param id the table primary key
     * @param employee the new employee
     * @return the updated employee on DB
     */
    public Employee updateEmployee(Long id, Employee employee) {
        Employee existingEmployee = employeeRepo.findById(id).orElse(null);
        if (existingEmployee != null) {
            existingEmployee.setLastname(employee.getLastname());
            existingEmployee.setFirstname(employee.getFirstname());
            //existingEmployee.setCost(employee.getCost());
            return employeeRepo.save(existingEmployee);
        }
        else{
            throw new IllegalStateException("Not found Employee with id:"+id);
        }
    }

    /**
     * Delete a employee from DB
     * @param id the table primary key
     */
    public Employee deleteEmployee(Long id) {
        boolean exists = employeeRepo.existsById(id);
        if (!exists){
            throw new IllegalStateException("Not found Employee with id:"+id);
        }
        else{
            Optional<Employee> employee = employeeRepo.findById(id);
            employeeRepo.deleteById(id);
            return employee.get();
        }

    }

    /**
     * Get all employees form DB
     * @return all the employees as a JSON list
     */
    public List<Employee> getAllEmployees(){
        return employeeRepo.findAll();
    }

    /**
     * Add new employee
     * @param employee
     * @return the new employee
     */
    public Employee addEmployee(Employee employee) {
        return employeeRepo.save(employee);
    }

}
