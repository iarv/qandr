package com.iarv.qar.repository;


import com.iarv.qar.dao.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query("select x from  Employee  x  where x.department.dname=:dname1")
    List<Employee> getAllEmployeesByDepartmentName(@Param("dname1") String dname);

    List<Employee> findAllByLastname(String lastname);
    List<Employee> findAllByJob(String job);
}
