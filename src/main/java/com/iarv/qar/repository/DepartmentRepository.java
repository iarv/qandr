package com.iarv.qar.repository;


import com.iarv.qar.dao.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
    @Query("select x from  Department  x  where x.location.locname=:locname1")
    List<Department > getAllDepartmentsByLocationName(@Param("locname1") String locname) ;
}
