package com.iarv.qar.dao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Set;

@Entity
@Table(name = "department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String dname;

    @OneToMany(mappedBy = "department", fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Employee> employees;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "loc_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Location location;

    public Department(Long id,
                      String dname,
                      Location location)
    {
        this.id = id;
        this.dname = dname;
        this.location = location;
    }

    public Department(String dname,
                      Location location)
    {
        this.dname = dname;
        this.location = location;
    }

    public Department() {
    }

    /**
     *
     * @return
     */
    public String getDname() {
        return dname;
    }

    /**
     *
     * @param dname
     */
    public void setDname(String dname) {
        this.dname = dname;
    }

    /**
     *
     * @return
     */
    public Set<Employee> getEmployees() {
        return employees;
    }

    /**
     *
     * @param employees
     */
    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    /**
     *
     * @return
     */
    public Location getLocation() {
        return location;
    }

    /**
     *
     * @param location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Printout the card values
     * @return the printout456
     */
    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", dname='" + dname + '\'' +
                ", employees=" + employees +
                ", location=" + location +
                '}';
    }
}