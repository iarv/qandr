package com.iarv.qar.dao;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "location")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String locname;
    @OneToMany(mappedBy = "location", fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JsonIgnore
    //@JoinColumn(name="dept_id", referencedColumnName ="id")
    private Set<Department> departments;


    public Location(Long id,
                    String locname)
    {
        this.id = id;
        this.locname = locname;
    }

    public Location(String locname)
    {
        this.locname = locname;
    }

    public Location() {
    }

    /**
     *
     * @return
     */
    public String getLocname() {
        return locname;
    }

    /**
     *
     * @param locname
     */
    public void setLocname(String locname) {
        this.locname = locname;
    }

    /**
     *
     * @return
     */
    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }

    /**
     * Printout the card values
     * @return the printout456
     */
    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", locname='" + locname + '\'' +
                ", departments=" + departments +
                '}';
    }
}

