package com.iarv.qar.dao;
import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String lastname;

    private String firstname;

    private String job;

    private int mngid;

    private LocalDate hiredate;

    //@Column(precision = 10, scale=2)
    private double salary;

    private double comm;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "dept_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    //@JsonIgnore
    private Department department;

    /**
     *
     * @param id
     * @param lastname
     * @param firstname
     * @param job
     * @param mngid
     * @param hiredate
     * @param salary
     * @param comm
     */
    public Employee(Long id,
                    String lastname,
                    String firstname,
                    String job,
                    int mngid,
                    LocalDate hiredate,
                    double salary,
                    double comm,
                    Department department) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.job = job;
        this.mngid = mngid;
        this.hiredate = hiredate;
        this.salary = salary;
        this.comm = comm;
        this.department = department;
    }

    /**
     *
     * @param lastname
     * @param firstname
     * @param job
     * @param mngid
     * @param hiredate
     * @param salary
     * @param comm
     */
    public Employee(String lastname,
                    String firstname,
                    String job,
                    int mngid,
                    LocalDate hiredate,
                    double salary,
                    double comm,
                    Department department) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.job = job;
        this.mngid = mngid;
        this.hiredate = hiredate;
        this.salary = salary;
        this.comm = comm;
        this.department = department;
    }

    public Employee() {
    }

    /**
     * getCountry
     * @return the country abbreviation
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * setCountry
     * @param lastname the country abbr
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     *
     * @return
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     *
     * @param firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     *
     * @return
     */
    public String getJob() {
        return job;
    }

    /**
     *
     * @param job
     */
    public void setJob(String job) {
        this.job = job;
    }

    /**
     *
     * @return
     */
    public int getMngid() {
        return mngid;
    }

    /**
     *
     * @param mngid
     */
    public void setMngid(int mngid) {
        this.mngid = mngid;
    }

    /**
     *
     * @return
     */
    public LocalDate getHiredate() {
        return hiredate;
    }

    /**
     *
     * @param hiredate
     */
    public void setHiredate(LocalDate hiredate) {
        this.hiredate = hiredate;
    }

    /**
     *
     * @return
     */
    public double getSalary() {
        return salary;
    }

    /**
     *
     * @param salary
     */
    public void setSalary(double salary) {
        //DecimalFormat df = new DecimalFormat(".##");
        this.salary = salary;//df.format(salary,".##");
    }

    /**
     *
     * @return
     */
    public double getComm() {
        return comm;
    }

    /**
     *
     * @param comm
     */
    public void setComm(double comm) {
        this.comm = comm;
    }

    /**
     *
     * @return
     */
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    /**
     * Printout the card values
     * @return the printout456
     */
    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", job='" + job + '\'' +
                ", mngid=" + mngid +
                ", hiredate=" + hiredate +
                ", salary=" + salary +
                ", comm=" + comm +
                ", department=" + department +
                '}';
    }
}

