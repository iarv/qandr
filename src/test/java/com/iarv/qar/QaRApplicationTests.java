package com.iarv.qar;

import com.iarv.qar.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
		properties = "spring.profiles.include=test")
class QaRApplicationTests {
	@LocalServerPort
	private int port;
	private String baseUrl="http://localhost";//+String.valueOf(port)+"+/qar";
	private static RestTemplate restTemplate;

	@Autowired
	private EmployeeRepository repo;

	@BeforeAll
	public static void init(){
		restTemplate = new RestTemplate();
	}

	@BeforeEach
	public void setUp(){
		baseUrl=baseUrl.concat(":").concat(String.valueOf(port)).concat("/qar");
	}

	@Test
	void contextLoads() {
	}


}
