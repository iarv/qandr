# Q & R

## Getting started
This is a case study provided to Q & R

### Configure environment
1) Install docker
2) Install MySQL via docker from main repo

   ```docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=password -d mysql```
3) Run backend application
4) Install postman to test the backend app (optional)

### Build application
1) Use Intellij and open the qar project
2) Build app

###Initial DB info
The app on startup create the qar DB and the tables employee,department,location.
It also sets the initial configuration values from file EmployeeConfig.json

### Manual checking backend application
In this app we support full CRUD operations from Employee,Department amd Location.
For sake of brevity we test here only the requested functionality by the case study

1) A) Request Get all locations
   
   URL: http://localhost:8090/getAllLocations to get all locations
      
   B) Response

         <List>
         <item>
         <locname>South</locname>
         </item>
         <item>
         <locname>North</locname>
         </item>
         </List>
       
2) A) Request Get all the departments per location name

   Set URL: http://localhost:8090/getAllDepartmentsByLocationName<locationName e.g. North>

   B) Response

         <List>
         <item>
         <dname>Software</dname>
         <location>
         <locname>South</locname>
         </location>
         </item>
         <item>
         <dname>Hardware</dname>
         <location>
         <locname>South</locname>
         </location>
         </item>
         <item>
         <dname>HR</dname>
         <location>
         <locname>South</locname>
         </location>
         </item>
         </List>

3) A) Request Get all the departments
   
   Set URL: http://localhost:8090/getAllDepartments

   B) Response

         <List>
         <item>
         <dname>Software</dname>
         <location>
         <locname>South</locname>
         </location>
         </item>
         <item>
         <dname>Hardware</dname>
         <location>
         <locname>South</locname>
         </location>
         </item>
         <item>
         <dname>HR</dname>
         <location>
         <locname>South</locname>
         </location>
         </item>
         <item>
         <dname>Software</dname>
         <location>
         <locname>North</locname>
         </location>
         </item>
         </List>
   
4) A) Request Get all the employees per department name
   
   Set URL: http://localhost:8090/getAllEmployeesByDepartmentName<departmentName e.g. Software>
   
   B) Response

         <List>
         <item>
         <lastname>Arvanitis</lastname>
         <firstname>John</firstname>
         <job>Developer</job>
         <mngid>12</mngid>
         <hiredate>2000-11-21</hiredate>
         <salary>2000.1234</salary>
         <comm>1000.0</comm>
         <department>
         <dname>Software</dname>
         <location>
         <locname>South</locname>
         </location>
         </department>
         </item>
         <item>
         <lastname>Katsoy</lastname>
         <firstname>Peter</firstname>
         <job>Developer</job>
         <mngid>12</mngid>
         <hiredate>2023-05-03</hiredate>
         <salary>1340.478</salary>
         <comm>1300.0</comm>
         <department>
         <dname>Software</dname>
         <location>
         <locname>South</locname>
         </location>
         </department>
         </item>
         <item>
         <lastname>Mpakothanashs</lastname>
         <firstname>Kyriakos</firstname>
         <job>Developer</job>
         <mngid>78</mngid>
         <hiredate>1999-11-21</hiredate>
         <salary>2145.1234</salary>
         <comm>1053.0</comm>
         <department>
         <dname>Software</dname>
         <location>
         <locname>North</locname>
         </location>
         </department>
         </item>
         </List>

5) Request Get all the employees
   
   Set URL: http://localhost:8090/getAllEmployees
   
   B) Response

            <List>
            <item>
            <lastname>Arvanitis</lastname>
            <firstname>John</firstname>
            <job>Developer</job>
            <mngid>12</mngid>
            <hiredate>2000-11-21</hiredate>
            <salary>2000.1234</salary>
            <comm>1000.0</comm>
            <department>
            <dname>Software</dname>
            <location>
            <locname>South</locname>
            </location>
            </department>
            </item>
            <item>
            <lastname>Katsoy</lastname>
            <firstname>Peter</firstname>
            <job>Developer</job>
            <mngid>12</mngid>
            <hiredate>2023-05-03</hiredate>
            <salary>1340.478</salary>
            <comm>1300.0</comm>
            <department>
            <dname>Software</dname>
            <location>
            <locname>South</locname>
            </location>
            </department>
            </item>
            <item>
            <lastname>Zouvelekis</lastname>
            <firstname>Kostas</firstname>
            <job>Developer</job>
            <mngid>17</mngid>
            <hiredate>2021-11-21</hiredate>
            <salary>2040.1234</salary>
            <comm>3000.0</comm>
            <department>
            <dname>Hardware</dname>
            <location>
            <locname>South</locname>
            </location>
            </department>
            </item>
            <item>
            <lastname>Koykos</lastname>
            <firstname>Andreas</firstname>
            <job>Developer</job>
            <mngid>6</mngid>
            <hiredate>2020-05-03</hiredate>
            <salary>3000.0</salary>
            <comm>1234.0</comm>
            <department>
            <dname>Hardware</dname>
            <location>
            <locname>South</locname>
            </location>
            </department>
            </item>
            <item>
            <lastname>Kokos</lastname>
            <firstname>Nikos</firstname>
            <job>Developer</job>
            <mngid>17</mngid>
            <hiredate>2020-05-03</hiredate>
            <salary>1040.478</salary>
            <comm>100.0</comm>
            <department>
            <dname>Hardware</dname>
            <location>
            <locname>South</locname>
            </location>
            </department>
            </item>
            <item>
            <lastname>Manetas</lastname>
            <firstname>Xristos</firstname>
            <job>Social Relations</job>
            <mngid>4</mngid>
            <hiredate>1998-11-21</hiredate>
            <salary>5000.1234</salary>
            <comm>1034.0</comm>
            <department>
            <dname>HR</dname>
            <location>
            <locname>South</locname>
            </location>
            </department>
            </item>
            <item>
            <lastname>Mpakothanashs</lastname>
            <firstname>Kyriakos</firstname>
            <job>Developer</job>
            <mngid>78</mngid>
            <hiredate>1999-11-21</hiredate>
            <salary>2145.1234</salary>
            <comm>1053.0</comm>
            <department>
            <dname>Software</dname>
            <location>
            <locname>North</locname>
            </location>
            </department>
            </item>
            </List>

6) A) Request Get all employees by lastname
 
   Set URL: http://localhost:8090/getAllEmployeesByLastName<lastName e.g. Arvanitis>
   
B) Response

         <List>
         <item>
         <lastname>Arvanitis</lastname>
         <firstname>John</firstname>
         <job>Developer</job>
         <mngid>12</mngid>
         <hiredate>2000-11-21</hiredate>
         <salary>2000.1234</salary>
         <comm>1000.0</comm>
         <department>
         <dname>Software</dname>
         <location>
         <locname>South</locname>
         </location>
         </department>
         </item>
         </List>

7) A) Request Get all employees by job

   Set URL: http://localhost:8090/getAllEmployeesByJob<jobName e.g. Developer>

   B) Response  

         <List>
         <item>
         <lastname>Arvanitis</lastname>
         <firstname>John</firstname>
         <job>Developer</job>
         <mngid>12</mngid>
         <hiredate>2000-11-21</hiredate>
         <salary>2000.1234</salary>
         <comm>1000.0</comm>
         <department>
         <dname>Software</dname>
         <location>
         <locname>South</locname>
         </location>
         </department>
         </item>
         <item>
         <lastname>Katsoy</lastname>
         <firstname>Peter</firstname>
         <job>Developer</job>
         <mngid>12</mngid>
         <hiredate>2023-05-03</hiredate>
         <salary>1340.478</salary>
         <comm>1300.0</comm>
         <department>
         <dname>Software</dname>
         <location>
         <locname>South</locname>
         </location>
         </department>
         </item>
         <item>
         <lastname>Zouvelekis</lastname>
         <firstname>Kostas</firstname>
         <job>Developer</job>
         <mngid>17</mngid>
         <hiredate>2021-11-21</hiredate>
         <salary>2040.1234</salary>
         <comm>3000.0</comm>
         <department>
         <dname>Hardware</dname>
         <location>
         <locname>South</locname>
         </location>
         </department>
         </item>
         <item>
         <lastname>Koykos</lastname>
         <firstname>Andreas</firstname>
         <job>Developer</job>
         <mngid>6</mngid>
         <hiredate>2020-05-03</hiredate>
         <salary>3000.0</salary>
         <comm>1234.0</comm>
         <department>
         <dname>Hardware</dname>
         <location>
         <locname>South</locname>
         </location>
         </department>
         </item>
         <item>
         <lastname>Kokos</lastname>
         <firstname>Nikos</firstname>
         <job>Developer</job>
         <mngid>17</mngid>
         <hiredate>2020-05-03</hiredate>
         <salary>1040.478</salary>
         <comm>100.0</comm>
         <department>
         <dname>Hardware</dname>
         <location>
         <locname>South</locname>
         </location>
         </department>
         </item>
         <item>
         <lastname>Mpakothanashs</lastname>
         <firstname>Kyriakos</firstname>
         <job>Developer</job>
         <mngid>78</mngid>
         <hiredate>1999-11-21</hiredate>
         <salary>2145.1234</salary>
         <comm>1053.0</comm>
         <department>
         <dname>Software</dname>
         <location>
         <locname>North</locname>
         </location>
         </department>
         </item>
         </List>
    
